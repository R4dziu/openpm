from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Project(models.Model):
    name = models.CharField(max_length=255)
    expiration_date = models.DateTimeField()
    description = models.TextField()
    requirements = models.TextField()
    owner = models.ForeignKey(User, on_delete = models.CASCADE)


class UsersToProjects(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    project = models.ForeignKey(Project, on_delete = models.CASCADE)
    status = models.TextField()

class UserDetails(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    description = models.TextField()
    skills = models.TextField()
    phone = models.TextField()

