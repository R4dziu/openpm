from rest_framework import serializers
from django.contrib.auth.models import User # If used custom user model
from . import models 

UserModel = User


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        user = UserModel(username=validated_data['username'])
        user.set_password(validated_data['password'])
        user.save()

        user_details = models.UserDetails(user=user)
        user_details.save()

        return user

    class Meta:
        model = UserModel
        # Tuple of serialized model fields (see link [2])
        fields = ( "id", "username", "password")


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Project
        fields = ('id', 'url', 'name', 'expiration_date', 'description', 'requirements')


class UsersToProjectsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.UsersToProjects
        fields = ('id', 'user', 'project', 'status')

class UserDetailsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.UserDetails
        fields = ('id', 'user', 'description', 'skills', 'phone')

class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModel
        fields = ('username','password')
        

