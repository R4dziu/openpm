from django.urls import path, include
from . import views

from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'users', views.CreateUserView)
router.register(r'projects', views.ProjectView)
router.register(r'userstoprojects', views.UsersToProjectsView)
router.register(r'userdetails', views.UserDetailsView)

urlpatterns = [
    # path('api/auth/', include('djoser.urls.authtoken')),
    path(r'api/', include((router.urls, 'app_name'))),
    path(r'register/', views.register_view , name='Register')
]
