from django.http import HttpResponse
from django.shortcuts import render
from rest_framework import (
    permissions,
    status
)
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView
from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import (
    get_user_model, 
    login,
    authenticate
)
from .serializers import UserSerializer, ProjectSerializer, UsersToProjectsSerializer, UserDetailsSerializer, LoginSerializer
from .models import Project, UsersToProjects, UserDetails


class CreateUserView(ModelViewSet):
    queryset = User.objects.all()
    model = get_user_model()
    permission_classes = [
        permissions.AllowAny # Or anon users can't register
    ]
    serializer_class = UserSerializer

class ProjectView(ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

    http_method_names = ['GET', 'POST']

    def get(self, request, format=None):
        return self.retrive(request)

    # def contents(self, request, *args, **kwargs):
    #     return self.retrive(request, *args, **kwargs)

class UsersToProjectsView(ModelViewSet):
    queryset = UsersToProjects.objects.all()
    serializer_class = UsersToProjectsSerializer

class UserDetailsView(ModelViewSet):
    queryset = UserDetails.objects.all()
    serializer_class = UserDetailsSerializer


# class UserLoginView(CreateAPIView):
#     queryset = User.objects.all()
#     serializer_class = LoginSerializer
#     # permission_classes = (permissions.IsAdminUser,)

#     def create(self, request, *args, **kwargs):
#         serializer = self.serializer_class()
#         data = serializer.data


#         return Response(status=status.HTTP_204_NO_CONTENT)

class UserLoginView(ModelViewSet):
    queryset = UserDetails.objects.all()
    serializer_class = LoginSerializer
    http_method_names = ['post']

    def create(self, request, *args, **kwargs):
        data = request.data
        username = data.get('username', None)
        password = data.get('password', None)
        user = authenticate(username=username, password=password)
    
        if user is not None:
            login(request, user)
            #TODO
            #context processort jak cart w moim gitlabie
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

def register_view(request, *args, **kwargs):
    # return HttpResponse("<h1>Hello World</h1>")

    return render(request, 'register.html', {})

